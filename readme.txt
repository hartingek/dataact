DataAct Report builder
v0.4 updated on 5/25/2021

Setup
1) To generate this report, pull the main repo to your local machine.
2) Update the batch file using a text editor (Notepad or Notepad++) to replace the <change to your username> field as well as the <change to your DataAct repo directory> field. 
3) Copy the batch file to an easily accessible location.

Running the report
1) Run the DataAct.bat file.
2) Click Browse and navigate to the files specified in the prompts.
3) Click Create Doc to finish the process.
4) The current day's file will be exported to the specified location.