#Setup
#vvvvvvvvvvvvv
#version 0.4
#vvvvvvvvvvvvv

import pandas as pd
#pd.options.mode.chained_assignment = None
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from datetime import date

testing = 0

#%%
#==========================
#parameter setup
#==========================
report_date = date.today().strftime("%B %d, %Y")
report_month = date.today().strftime('%m')
report_year = date.today().strftime('%y')

#Quarter and Period calculator
if 10 <= int(report_month) <= 12:
    report_quarter = 'Q1'
elif 1 <= int(report_month) <= 3:
    report_quarter = 'Q2'
elif 4 <= int(report_month) <= 6:
    report_quarter = 'Q3'
else:
    report_quarter = 'Q4'

#Period calculator
if 1 <= int(report_month) <= 7:
    report_period = '0'+str(int(report_month) + 2)
elif 8 <= int(report_month) <= 10:
    report_period = str(int(report_month) + 2)
else:
    report_period = '0'+str(int(report_month) - 10)

#FY calculator
if report_quarter == 'Q1':
    report_fy = str(int(date.today().strftime('%y'))+1)
else:
    report_fy = date.today().strftime('%y')

filename = f'{report_quarter} FY{report_fy} PD{report_period} Overseas Reconciliation as of {date.today().strftime("%Y%m%d")}'
C_reportables_lst = ['','Match - No Variance','Match - Variance','FPDS Rejection','In FPDS Draft Status','In FPDS, not in D1','Miscellaneous','Timing']
D1_reportables_lst = ['','Match - No Variance','Match - Variance','Incorrect RFMS Document Type','Not Found in RFMS','Other Agency Funded','Timing']


#%%
#==========================
#tkinter setup
#==========================

if testing == 0:
    main = tk.Tk()
    main.geometry('500x300') 
    main.title('DataAct File Builder')
    
    #frame
    frame = tk.Frame(main,padx=5, pady=5, width=500, height=300)
    
    #style
    frame.grid_rowconfigure(0, minsize=15)
    frame.grid_rowconfigure(1, minsize=15)
    for row in range(2,8):
        frame.grid_rowconfigure(row, minsize=30)
    frame.grid_rowconfigure(8, minsize=60)
    
    style = ttk.Style()
    style.configure('BWTLabel', foreground="black", background="white")
    ttk.Style().configure("TButton", padding=6, relief="flat",   background="#ccc")
    
    #functions
    #get path from user, set label 
    def getReconImport():
        global recon_import_path, recon_import_path
        recon_import_path = filedialog.askopenfilename()
        tk.Label(frame, text=recon_import_path,justify=tk.LEFT, wraplength=300).grid(row=0, column = 1) 
    def getCrswlkImport():
        global crswlk_import_path, crswlk_import_path
        crswlk_import_path = filedialog.askopenfilename()
        tk.Label(frame, text=crswlk_import_path,justify=tk.LEFT, wraplength=300).grid(row=2, column = 1) 
    def getFileExport():
        global export_path
        export_path = filedialog.asksaveasfilename(defaultextension='.xlsx', filetypes=(('Excel','*.xlsx'), ("All Files", "*.*")), initialfile = filename)
        tk.Label(frame, text=export_path,justify=tk.LEFT, wraplength=300).grid(row=4, column = 1) 
    def closeUI():
        main.destroy()
    
    #input and export filenames
    lblReconFile = tk.Label(frame, text= 'Recon File:').grid(row=0, column = 0, sticky= tk.SW) 
    btnReconFile = ttk.Button(frame, text='Browse...', command=getReconImport, width=16).grid(row=1, column = 0, sticky= tk.SW) 
    
    lblCrswlkFile = tk.Label(frame, text= 'Crosswalk File:').grid(row=2, column = 0, sticky= tk.SW) 
    btnCrswlkFile = ttk.Button(frame, text='Browse...', command=getCrswlkImport, width=16).grid(row=3, column = 0, sticky= tk.SW) 
    
    lblSave = tk.Label(frame, text= 'Output File:').grid(row=4, column = 0, sticky= tk.SW) 
    btnSave = ttk.Button(frame, text='Browse...', command=getFileExport, width=16).grid(row=5, column = 0, sticky= tk.SW) 
    
    btnCreate = ttk.Button(frame, text = 'Create Doc', command=closeUI, width=16).grid(row=7, column=0, sticky= tk.SW)
    frame.grid(row=0,column=0)
    main.mainloop()


else:
#==========================
#==========================
#Test Setup Section
    recon_import_path = 'C:/Users/eharting/Documents/Programming/DataAct/Overseas_Recon20210421.xlsx'
    crswlk_import_path = 'C:/Users/eharting/Documents/Programming/DataAct/FY21 PD6 Crosswalk as of 4.12.xlsx'
    export_path = f'C:/Users/eharting/Documents/Programming/DataAct/{filename}.xlsx'
    print('\n%%%\n%%%\n%%%\nin test mode\n%%%\n%%%\n%%%')
#==========================
#==========================




#%%
#==========================
#Dataframe Setup
#==========================
writer = pd.ExcelWriter(export_path, engine='xlsxwriter')
workbook = writer.book
format_acct = workbook.add_format({'num_format':'_($* #,##0.00_);_($* (#,##0.00);_($* "-"??_);_(@_)'})
format_pct  = workbook.add_format({'num_format':'0.00%'})
format_merge = workbook.add_format({'bold':True,'font_size':20,'align':'center','valign':'vcenter','fg_color':'#002060','font_color':'#ffffff'})
format_highlight = workbook.add_format({'fg_color':'#FFFF00'})
format_sum = workbook.add_format({'fg_color':'#DDEBF7'})

def get_col_widths(df):
    # First we find the maximum length of the index column   
    idx_max = max([len(str(s)) for s in df.index.values] + [len(str(df.index.name))])
    # Then, we concatenate this to the max of the lengths of column name and its values for each column, left to right
    return [idx_max] + [max([len(str(s)) for s in df[col].values] + [len(col)]) for col in df.columns]

recon_df = pd.read_excel(recon_import_path,'Data')
recon_df = recon_df.iloc[:,:-2]
recon_df = recon_df.replace({'Match- Variance':'Match - Variance', 'Match- No Variance':'Match - No Variance'})

recon_df1 = recon_df.copy()

#excluding Crosswalk data
try:
    crswlk_import_path
except NameError:
    pass
else:
    crswlk_health_df = pd.read_excel(crswlk_import_path,'Healthcare')
    crswlk_oaf_df = pd.read_excel(crswlk_import_path,'0% DOS Funding')
    crswlk_oaf_df = crswlk_oaf_df.append(pd.read_excel(crswlk_import_path,'Partial DOS Funding')).reset_index()
    
    crswlk_health_lst = crswlk_health_df['Order_ID '].tolist()
    crswlk_oaf_lst = crswlk_oaf_df['Order_ID '].tolist()



#%%
#==========================
#Building Tabs
#==========================
#Build Overseas Tabs
#---------------------------
#D1 to C
recon_df1.drop(recon_df1[recon_df1['MISSING_REASON_CODE']=='Other Agency Funded'].index, inplace=True)
try:
    crswlk_import_path
except NameError:
    pass
else:
    recon_df1.drop(recon_df1[recon_df1['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_health_lst)].index, inplace=True)
    recon_df1.drop(recon_df1[recon_df1['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_oaf_lst)].index, inplace=True)
    
    
pivotD1 = recon_df1[recon_df1['MISSING_REASON_CODE'].isin(D1_reportables_lst) & 
               ((recon_df1['EXISTS_IN_FILE'] == 'BOTH') | 
                (recon_df1['EXISTS_IN_FILE'] == 'File D1 Only') |
                (recon_df1['EXISTS_IN_FILE'] == 'Total'))]
pivotD11 = pivotD1.pivot_table(index=['EXISTS_IN_FILE','MISSING_REASON_CODE'], 
                       values=['OBLIGATION_DOCUMENT_NUMBER'], 
                       aggfunc=['count'],
                       margins = True,
                       margins_name = 'Grand Total')
pivotD12 = pivotD1.pivot_table(index=['EXISTS_IN_FILE','MISSING_REASON_CODE'], 
                       values=['CURRENT_PERIOD_OBL_AMOUNT','TOTAL_D_AMOUNT_CURRENT_PERIOD','TOTAL_VARIANCE_CURRENT_PERIOD'], 
                       aggfunc=['sum'],
                       margins = True,
                       margins_name = 'Grand Total')

pivotD1 = pd.merge(pivotD11, pivotD12,'left', on =['EXISTS_IN_FILE','MISSING_REASON_CODE'])
pivotD1.columns=['Count of PIIDS', 'Total C','Total D1','Total Variance']

pivotD1['PD'+str(report_period)+' % Abs(Var/Total D1)'] = abs(pivotD1['Total Variance']) / pivotD1.iloc[-1]['Total D1']
pdLessOne = 'PD'+str(int(report_period)-1)+'% Abs(Var/Total D1)'
pdLessTwo = 'PD'+str(int(report_period)-2)+'% Abs(Var/Total D1)'

pivotD1[['pdLessOne','pdLessTwo','Notes']] = ''
pivotD1 = pivotD1.rename(columns={'pdLessOne':pdLessOne,'pdLessTwo':pdLessTwo})
pivotD1 = pivotD1.rename_axis(['Exists in File','Missing Reason']).reset_index()

pivotD1.to_excel(writer, sheet_name='Overseas D1 to C', index=False, startrow=1)
D1_worksheet = writer.sheets['Overseas D1 to C']

D1_worksheet.set_column('D:F', None , format_acct)
D1_worksheet.set_column('G:I', None , format_pct)

for i, width in enumerate(get_col_widths(pivotD1)):
    D1_worksheet.set_column(i-1, i-1, width)

D1_worksheet.merge_range('A1:J1', f'{report_quarter} FY{report_fy} Period {report_period} Overseas Procurement Reconciliation (as of {report_date}) - File D1 to C', format_merge)

#%%
#---------------------------
#D1 to C Analysis

#should be accurate... changed the recon_df to recon_df1 in the query portion
DCAnalysis_df = recon_df1[recon_df1['MISSING_REASON_CODE'].isin(D1_reportables_lst) &
                         ((recon_df1['EXISTS_IN_FILE'] == 'BOTH' ) | 
                          (recon_df1['EXISTS_IN_FILE'] == 'File D1 Only' ))]

DCAnalysis_df = DCAnalysis_df.sort_values(['EXISTS_IN_FILE','MISSING_REASON_CODE','TOTAL_VARIANCE_ABS'], 
                                          ascending=False).groupby('MISSING_REASON_CODE').head(25)
DCAnalysis_df = DCAnalysis_df[['EXISTS_IN_FILE','MISSING_REASON_CODE','OBLIGATION_DOCUMENT_NUMBER','TOTAL_VARIANCE_ABS']]

DCAnalysis_df.to_excel(writer, sheet_name='DELETE - D1 to C Analysis', index=False)
DCAnalysis_worksheet = writer.sheets['DELETE - D1 to C Analysis']

DCAnalysis_worksheet.set_column('D:D', None , format_acct)
DCAnalysis_worksheet.autofilter('A1:D'+str(len(DCAnalysis_df)+1))

#set column width
for i, width in enumerate(get_col_widths(DCAnalysis_df)):
    DCAnalysis_worksheet.set_column(i-1, i-1, width)



#%%
#---------------------------
#C to D1
pivotC = recon_df[recon_df['MISSING_REASON_CODE'].isin(C_reportables_lst) & 
               ((recon_df['EXISTS_IN_FILE'] == 'BOTH') | 
                (recon_df['EXISTS_IN_FILE'] == 'File C Only') |
                (recon_df['EXISTS_IN_FILE'] == 'Total'))]
pivotC1 = pivotC.pivot_table(index=['EXISTS_IN_FILE','MISSING_REASON_CODE'], 
                       values=['OBLIGATION_DOCUMENT_NUMBER'], 
                       aggfunc=['count'],
                       margins = True,
                       margins_name = 'Grand Total')
pivotC2 = pivotC.pivot_table(index=['EXISTS_IN_FILE','MISSING_REASON_CODE'], 
                       values=['CURRENT_PERIOD_OBL_AMOUNT','TOTAL_D_AMOUNT_CURRENT_PERIOD','TOTAL_VARIANCE_CURRENT_PERIOD'], 
                       aggfunc=['sum'],
                       margins = True,
                       margins_name = 'Grand Total')

pivotC = pd.merge(pivotC1, pivotC2,'left', on =['EXISTS_IN_FILE','MISSING_REASON_CODE'])
pivotC.columns=['Count of PIIDS', 'Total C','Total D1','Total Variance']

pivotC['PD'+str(report_period)+' % Abs(Var/Total C)'] = abs(pivotC['Total Variance']) / pivotC.iloc[-1]['Total C']
pdLessOne = 'PD'+str(int(report_period)-1)+'% Abs(Var/Total C)'
pdLessTwo = 'PD'+str(int(report_period)-2)+'% Abs(Var/Total C)'

pivotC[['pdLessOne','pdLessTwo','Notes']] = ''
pivotC = pivotC.rename(columns={'pdLessOne':pdLessOne, 'pdLessTwo':pdLessTwo})
pivotC = pivotC.rename_axis(['Exists in File','Missing Reason']).reset_index()


pivotC.to_excel(writer, sheet_name='Overseas C To D1', index=False, startrow=1)
C_worksheet = writer.sheets['Overseas C To D1']

C_worksheet.set_column('D:F', None , format_acct)
C_worksheet.set_column('G:I', None , format_pct)

for i, width in enumerate(get_col_widths(pivotC)):
    C_worksheet.set_column(i-1, i-1, width)

C_worksheet.merge_range('A1:J1', f'{report_quarter} FY{report_fy} Period {report_period} Overseas Procurement Reconciliation (as of {report_date}) - File C to D1', format_merge)



#%%
#---------------------------
#C to D1 Analysis
CDAnalysis_df = recon_df[recon_df['MISSING_REASON_CODE'].isin(C_reportables_lst) &
                         ((recon_df['EXISTS_IN_FILE'] == 'BOTH' ) | 
                          (recon_df['EXISTS_IN_FILE'] == 'File C Only' ))]

CDAnalysis_df = CDAnalysis_df.sort_values(['EXISTS_IN_FILE','MISSING_REASON_CODE','TOTAL_VARIANCE_ABS'], 
                                          ascending=False).groupby('MISSING_REASON_CODE').head(25)
CDAnalysis_df = CDAnalysis_df[['EXISTS_IN_FILE','MISSING_REASON_CODE','OBLIGATION_DOCUMENT_NUMBER','TOTAL_VARIANCE_ABS']]

CDAnalysis_df.to_excel(writer, sheet_name='DELETE - C to D1 Analysis', index=False)
CDAnalysis_worksheet = writer.sheets['DELETE - C to D1 Analysis']

CDAnalysis_worksheet.set_column('D:D', None , format_acct)
CDAnalysis_worksheet.autofilter('A1:D'+str(len(CDAnalysis_df)+1))

#set column width
for i, width in enumerate(get_col_widths(CDAnalysis_df)):
    CDAnalysis_worksheet.set_column(i-1, i-1, width)



#%%
#==========================
#Build Data Tab
#==========================
#excluding Crosswalk data
recon_df1.drop(recon_df1[recon_df1['MISSING_REASON_CODE']=='Other Agency Funded'].index, inplace=True)
try:
    crswlk_import_path
except NameError:
    pass
else:
    recon_df1.drop(recon_df1[recon_df1['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_health_lst)].index, inplace=True)
    recon_df1.drop(recon_df1[recon_df1['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_oaf_lst)].index, inplace=True)

recon_df1.to_excel(writer, sheet_name='Data', index=False)
Data_worksheet = writer.sheets['Data']

#set column width
for i, width in enumerate(get_col_widths(recon_df1)):
    Data_worksheet.set_column(i-1, i-1, width)



#%%
#==========================
#Build Health-Life Medical Tab
#==========================
#excluding Crosswalk data
try:
    crswlk_import_path
except NameError:
    pass
else:
    recon_df2 = recon_df[recon_df['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_health_lst)]
    recon_df2['MISSING_REASON_CODE'] = 'Health-Life-Medical'
    recon_df2.to_excel(writer, sheet_name='Health-Life-Medical', index=False)
    Health_worksheet = writer.sheets['Health-Life-Medical']
    
    #set column width
    for i, width in enumerate(get_col_widths(recon_df2)):
        Health_worksheet.set_column(i-1, i-1, width)



#%%
#==========================
#Build Other Agency Funded Tab
#==========================
#excluding Crosswalk data
try:
    crswlk_import_path
except NameError:
    pass
else:
    recon_df3 = recon_df[recon_df['MISSING_REASON_CODE'] == 'Other Agency Funded']
    oaf_df = recon_df[recon_df['OBLIGATION_DOCUMENT_NUMBER'].isin(crswlk_oaf_lst)]
    recon_df3 = recon_df3.append(oaf_df)
    recon_df3['MISSING_REASON_CODE'] = 'Other Agency Funded'
    recon_df3.to_excel(writer, sheet_name='Other Agency Funded', index=False)
    Other_worksheet = writer.sheets['Other Agency Funded']
    
    #set column width
    for i, width in enumerate(get_col_widths(recon_df3)):
        Other_worksheet.set_column(i-1, i-1, width)



#%%
#==========================
#Large Variance Analysis Tab
#==========================
#Variance tab testing
variance_df = recon_df.loc[((recon_df['EXISTS_IN_FILE'] == 'File C Only') |
                               (recon_df['EXISTS_IN_FILE'] == 'BOTH')) &
                              ((recon_df['MISSING_REASON_CODE'] == 'Match - Variance') |
                               (recon_df['MISSING_REASON_CODE'] == 'Timing') |
                               (recon_df['MISSING_REASON_CODE'] == 'FPDS Rejection'))]

variance_df.insert(3,'Notes','')
variance_df.insert(4,'POC Action','')
variance_df = variance_df.sort_values(['TOTAL_VARIANCE_ABS'], ascending=False).head(20)

variance_df.to_excel(writer, sheet_name='Large Variance Analysis', index=False)

Variance_worksheet = writer.sheets['Large Variance Analysis']

for i, width in enumerate(get_col_widths(variance_df)):
    Variance_worksheet.set_column(i-1, i-1, width)

Variance_worksheet.set_column('M:M', None , format_acct)
#Variance_worksheet.activate()


#%%
#Save file
writer.save()

print('\n\n*Ding* Your file is ready\n\n')